### 表 ###
DROP TABLE IF EXISTS `bbs_bible`;
CREATE TABLE `bbs_bible` (
  volume_id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '卷编号',
  volume_name char(32) NOT NULL DEFAULT '' COMMENT '名称',	# 不可以重复
  volume_screen_name char(32) NOT NULL DEFAULT '' COMMENT '显示名称',	# 真实姓名，天朝预留
  chapter_count int(10) unsigned NOT NULL DEFAULT '0' COMMENT '章数',
  PRIMARY KEY (volume_id),
  KEY volume_name (volume_name)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `bbs_bible_content`;
CREATE TABLE `bbs_bible_content` (
  content_id int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '内容编号',
  volume_id int(10) unsigned NOT NULL DEFAULT '0' COMMENT '卷编号',
  volume_name char(32) NOT NULL DEFAULT '' COMMENT '名称',	# 不可以重复
  volume_screen_name char(32) NOT NULL DEFAULT '' COMMENT '显示名称',	# 真实姓名，天朝预留
  chapter_id int(10) unsigned NOT NULL DEFAULT '0' COMMENT '章编号',
  section_id int(10) unsigned NOT NULL DEFAULT '0' COMMENT '节编号',
  content varchar(255) NOT NULL DEFAULT '' COMMENT '内容',				# 内容
  PRIMARY KEY (content_id),
  KEY volume_name (volume_name),
  KEY chapteridsectionid (chapter_id, section_id),
  KEY content (content)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
