<?php


!defined('DEBUG') AND exit('Access Denied.');
$action = param('1');
empty($action) and $action = 'index';
switch($action) {
case 'create':
$volume_name = param('volume_name');
$chapter_id = param('chapter_id');
$section_id = param('section_id');
$content = param('content');
(empty($volume_name) or empty($chapter_id) or empty($section_id) or empty($content)) and exit('no params');
$bi = db_find_one('bible', array('volume_name'=>$volume_name));
$volume_screen_name = $bi['volume_screen_name'];
$volume_id = $bi['volume_id'];
$data = array();
$data['volume_id'] = $volume_id;
$data['volume_name'] = $volume_name;
$data['volume_screen_name'] = $volume_screen_name;
$data['chapter_id'] =$chapter_id;
$data['section_id']=$section_id;
$data['content']=$content;
$ret = db_create('bible_content', $data);
echo 'the result is:' .$ret;
break;
case 'create_cid':
$volume_name = param(2);
$ct = param(3);
$ret = db_update('bible', array('volume_name'=>$volume_name), array('chapter_count'=>$ct));
echo 'the chapter count changed:' .$ret;
break;
case 'index':
$bible_index = db_find('bible', array(), array(), 1, 100);
$html = '<!DOCTYPE html>
<html>
<head>
<title>圣经</title>
</head>
<body>';
foreach($bible_index as $index) {
if($index['volume_id'] == 1) {
$html.='<h2>就约</h2>';
}
if($index['volume_id'] == 40) {
$html.='<h2>新约</h2>';
}
$html.='<a href="bible-volume-'.$index['volume_name'].'.htm">'.$index['volume_screen_name'].'</a>';
}
$html .='</body>
</html>';
echo $html;
break;
case 'volume':
$volume_name = param(2);
$volume = db_find_one('bible', array('volume_name'=>$volume_name));
$html = '<!DOCTYPE html>
<html>
<head>
<title>圣经</title>
</head>
<body>';
$html.='<h2>'.$volume['volume_screen_name'].'</h2>';
for($i = 0; $i < $volume['chapter_count']; $i++) {
$j = $i + 1;
$html.='<a href="bible-chapter.htm?volume='.$volume_name.'&chapter_id='.$j.'">第 '.$j.' 章</a>';
}
$html .='</body>
</html>';
echo $html;
break;
case 'chapter':
$volume_name = param('volume');
$chapter_id = param('chapter_id');
$cl = db_find('bible_content', array('volume_name'=>$volume_name, 'chapter_id'=>$chapter_id), array(), 1, 1000);
$ct = '<ol>';
foreach($cl as $c) {
$ct.= '<li>' . $c['content'] . '</li>';
}
$ct.='</ol>';
show_html('', $ct);
break;
case 'delete':
$ret = db_delete('bible_content', array('volume_id'=>0));
echo $ret;
break;
case 't':
$bi = db_find('bible', array(), array(), 1, 2);
print_r($bi);
$list = db_find('bible_content', array(), array(), 1, 10);
print_r($list);
break;
default:
break;
}
function show_html($title, $content) {
$html = '<!DOCTYPE html>
<html>
<head>
<title>'.$title.'</title>
</head>
<body>
<h2>'.$title.'</h2>
'.$content.'</body
</html>';
echo $html;
}
