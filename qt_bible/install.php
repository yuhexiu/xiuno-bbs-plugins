<?php

/*
	Xiuno BBS 4.0 插件：圣经插件安装
	admin/plugin-install-qt_bible.htm
*/

!defined('DEBUG') AND exit('Forbidden');
install_sql_file(APP_PATH . 'plugin/bible/install_bible.sql');
$bible_index = array(
array("volume_name"=>"Genesis", "volume_screen_name"=>"创世记"),
array("volume_name"=>"Exodus", "volume_screen_name"=>"出埃及记"),
array("volume_name"=>"Leviticus", "volume_screen_name"=>"利未记"),
array("volume_name"=>"Numbers", "volume_screen_name"=>"民数记"),
array("volume_name"=>"Deuteronomy", "volume_screen_name"=>"申命记"),
array("volume_name"=>"Joshua", "volume_screen_name"=>"约书亚记"),
array("volume_name"=>"Judges", "volume_screen_name"=>"士师记"),
array("volume_name"=>"Ruth", "volume_screen_name"=>"路得记"),
array("volume_name"=>"1_Samuel", "volume_screen_name"=>"撒母耳记上"),
array("volume_name"=>"2_Samuel", "volume_screen_name"=>"撒母耳记下"),
array("volume_name"=>"1_Kings", "volume_screen_name"=>"列王纪上"),
array("volume_name"=>"2_Kings", "volume_screen_name"=>"列王纪下"),
array("volume_name"=>"1_Chronicles", "volume_screen_name"=>"历代志上"),
array("volume_name"=>"2_Chronicles", "volume_screen_name"=>"历代志下"),
array("volume_name"=>"Ezra", "volume_screen_name"=>"以斯拉记"),
array("volume_name"=>"Nehemiah", "volume_screen_name"=>"尼希米记"),
array("volume_name"=>"Esther", "volume_screen_name"=>"以斯帖记"),
array("volume_name"=>"Job", "volume_screen_name"=>"约伯记"),
array("volume_name"=>"Psalm", "volume_screen_name"=>"诗篇"),
array("volume_name"=>"Proverbs", "volume_screen_name"=>"箴言"),
array("volume_name"=>"Ecclesiastes", "volume_screen_name"=>"传道书"),
array("volume_name"=>"Song_of_Songs", "volume_screen_name"=>"雅歌"),
array("volume_name"=>"Isaiah", "volume_screen_name"=>"以赛亚书"),
array("volume_name"=>"Jeremiah", "volume_screen_name"=>"耶利米书"),
array("volume_name"=>"Lamentations", "volume_screen_name"=>"耶利米哀歌"),
array("volume_name"=>"Ezekiel", "volume_screen_name"=>"以西结书"),
array("volume_name"=>"Daniel", "volume_screen_name"=>"但以理书"),
array("volume_name"=>"Hosea", "volume_screen_name"=>"何西阿书"),
array("volume_name"=>"Joel", "volume_screen_name"=>"约珥书"),
array("volume_name"=>"Amos", "volume_screen_name"=>"阿摩司书"),
array("volume_name"=>"Obadiah", "volume_screen_name"=>"俄巴底亚书"),
array("volume_name"=>"Jonah", "volume_screen_name"=>"约拿书"),
array("volume_name"=>"Micah", "volume_screen_name"=>"弥迦书"),
array("volume_name"=>"Nahum", "volume_screen_name"=>"那鸿书"),
array("volume_name"=>"Habakkuk", "volume_screen_name"=>"哈巴谷书"),
array("volume_name"=>"Zephaniah", "volume_screen_name"=>"西番雅书"),
array("volume_name"=>"Haggai", "volume_screen_name"=>"哈该书"),
array("volume_name"=>"Zechariah", "volume_screen_name"=>"撒迦利亚书"),
array("volume_name"=>"Malachi", "volume_screen_name"=>"玛拉基书"),
array("volume_name"=>"Matthew", "volume_screen_name"=>"马太福音"),
array("volume_name"=>"Mark", "volume_screen_name"=>"马可福音"),
array("volume_name"=>"Luke", "volume_screen_name"=>"路加福音"),
array("volume_name"=>"John", "volume_screen_name"=>"约翰福音"),
array("volume_name"=>"Acts", "volume_screen_name"=>"使徒行传"),
array("volume_name"=>"Romans", "volume_screen_name"=>"罗马书"),
array("volume_name"=>"1_Corinthians", "volume_screen_name"=>"哥林多前书"),
array("volume_name"=>"2_Corinthians", "volume_screen_name"=>"哥林多后书"),
array("volume_name"=>"Galatians", "volume_screen_name"=>"加拉太书"),
array("volume_name"=>"Ephesians", "volume_screen_name"=>"以弗所书"),
array("volume_name"=>"Philippians", "volume_screen_name"=>"腓立比书"),
array("volume_name"=>"Colossians", "volume_screen_name"=>"歌罗西书"),
array("volume_name"=>"1_Thessalonians", "volume_screen_name"=>"帖撒罗尼迦前书"),
array("volume_name"=>"2_Thessalonians", "volume_screen_name"=>"帖撒罗尼迦后书"),
array("volume_name"=>"1_Timothy", "volume_screen_name"=>"提摩太前书"),
array("volume_name"=>"2_Timothy", "volume_screen_name"=>"提摩太后书"),
array("volume_name"=>"Titus", "volume_screen_name"=>"提多书"),
array("volume_name"=>"Philemon", "volume_screen_name"=>"腓利门书"),
array("volume_name"=>"Hebrews", "volume_screen_name"=>"希伯来书"),
array("volume_name"=>"James", "volume_screen_name"=>"雅各书"),
array("volume_name"=>"1_Peter", "volume_screen_name"=>"彼得前书"),
array("volume_name"=>"2_Peter", "volume_screen_name"=>"彼得后书"),
array("volume_name"=>"1_John", "volume_screen_name"=>"约翰一书"),
array("volume_name"=>"2_John", "volume_screen_name"=>"约翰二书"),
array("volume_name"=>"3_John", "volume_screen_name"=>"约翰三书"),
array("volume_name"=>"Jude", "volume_screen_name"=>"犹大书"),
array("volume_name"=>"Revelation", "volume_screen_name"=>"启示录"),
);
foreach($bible_index as $bible) {
db_create('bible', $bible);
}

message(1, 'ok');
function install_sql_file($sqlfile) {
	global $errno, $errstr;
	$s = file_get_contents($sqlfile);
	//$s = str_replace(";\r\n", ";\n", $s);
	$s = preg_replace('/#(.*?)\r\n/i', "", $s);
	$arr = explode(";\r\n", $s);
	foreach ($arr as $sql) {
		$sql = trim($sql);
		if(empty($sql)) continue;
		$arr = explode(";\n", $s);
		db_exec($sql) === FALSE AND message(-1, "sql: $sql, errno: $errno, errstr: $errstr");
	}
}

