<?php exit;
	global $conf, $uid, $g_static_users;
	if($conf['cache']['type'] == 'mysql') {
		$uids = array();
		foreach($threadlist as &$thread) {
			if($uid != $thread['uid']) {
				$uids[] = $thread['uid'];
			}
			if(!empty($thread['lastuid']) && $thread['lastuid'] != $thread['uid'] && $thread['lastuid'] != $uid) {
				$uids[] = $thread['lastuid'];
			}
		}
		$uids = array_unique($uids);
		$ul = empty($uids) ? array() : user_find(array('uid'=>$uids), array(), 1, 1000);
		if(!empty($ul)) {
			foreach($ul as &$u) {
				user_format($u);
				$g_static_users[$u['uid']] = $u;
			}
		}
	}
