<?php exit;
	global $conf, $uid, $g_static_users;
	if($conf['cache']['type'] == 'mysql') {
		$uids = array();
		foreach($postlist as &$post) {
			if($post['uid'] == $uid || $post['uid'] == 0) {
				continue;
			}
			$uids[] = $post['uid'];
		}
		$uids = array_unique($uids);
		$ul = empty($uids) ? array() : user_find(array('uid'=>$uids), array(), 1, 1000);
		if(!empty($ul)) {
			foreach($ul as &$u) {
				user_format($u);
				$g_static_users[$u['uid']] = $u;
			}
		}
	}
