<?php

/*
	Xiuno BBS 4.0 插件：回帖可见性安装
	admin/plugin-install-qt_reply_visibility.htm
*/

!defined('DEBUG') AND exit('Forbidden');

$tablepre = $db->tablepre;
$sql = "ALTER TABLE {$tablepre}thread ADD COLUMN visibility int(10) NOT NULL default '0';";
db_exec($sql);
