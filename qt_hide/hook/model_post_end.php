<?php exit;
function process_hide($message) {
global $uid;

$r = '以下内容需要登录后查看。如果您已经有账号，请<a href="'.url('user-login').'">点击这里登录</a>，如果您还没有账号，请先<a href="'.url('user-create').'">点击这里注册</a>';
if($uid == 0) {
$message = preg_replace('#\[hide\].*?\[/hide\]#is', $r, $message);
} else {
$message = preg_replace('#\[hide\](.*?)\[/hide\]#is', '\1', $message);
}
return $message;
}
