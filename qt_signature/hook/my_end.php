elseif($action == 'signature') {
	user_login_check();
	if($method == 'GET') {
		
		include _include(APP_PATH.'plugin/qt_signature/view/htm/my_signature.htm');
		
	} elseif($method == 'POST') {
		
		
		$signature = param('signature');
		empty($signature) AND message('signature', lang('please_signature'));
		$signature = htmlspecialchars($signature);
		$r = user_update($uid, array('signature'=>$signature));
		$r === FALSE AND message(-1, lang('signature_modify_failed'));
		
		message(0, lang('signature_modify_successfully'));
		
	}
	
}