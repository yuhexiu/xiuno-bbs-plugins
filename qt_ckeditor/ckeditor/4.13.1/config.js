/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	config.startupFocus = true;
	config.extraPlugins = ['divarea', 'html5audio'];
	config.removePlugins = 'forms';
	config.removeButtons = 'Flash';
	//config.title = '内容';
	//config.autoUpdateElement = true;
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
};
