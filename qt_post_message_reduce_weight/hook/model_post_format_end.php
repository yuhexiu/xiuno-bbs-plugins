<?php exit;
	if(empty($post['message_fmt'])) {
	// 格式转换: 类型，0: html, 1: txt; 2: markdown; 3: ubb
		$post['message_fmt'] = htmlspecialchars($post['message']);
		$post['doctype'] == 0 && $post['message_fmt'] = ($gid == 1 ? $post['message'] : xn_html_safe($post['message']));
		$post['doctype'] == 1 && $post['message_fmt'] = xn_txt_to_html($post['message']);
	$post['doctype'] == 2 && function_exists('markdown2html') && $post['message_fmt'] = markdown2html($post['message']);
		// 对引用进行处理
		!empty($post['quotepid']) && $post['quotepid'] > 0 && $post['message_fmt'] = post_quote($post['quotepid']).$post['message_fmt'];

	}
